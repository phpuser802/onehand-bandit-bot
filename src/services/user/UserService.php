<?php


namespace app\botCore\services;


use app\botCore\dto\update\UserDto;
use app\botCore\dto\user\RouteDto;
use app\botCore\dto\user\UserDto as DbUserDto;
use app\botCore\interfaces\repositories\RouteRepositoryInterface;
use app\botCore\interfaces\repositories\UserRepositoryInterface;
use app\botCore\traits\MagicGetSetTrait;
use Spatie\DataTransferObject\Exceptions\UnknownProperties;

/**
 * Class UserService
 * @package app\bptCore\services
 */
class UserService
{
    use MagicGetSetTrait;

    /**
     * @var UserRepositoryInterface
     */
    private UserRepositoryInterface $userRepository;

    /**
     * @var RouteRepositoryInterface
     */
    private RouteRepositoryInterface $routeRepository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $userRepository
     * @param RouteRepositoryInterface $routeRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository,
        RouteRepositoryInterface $routeRepository,
    )
    {
        $this->userRepository = $userRepository;
        $this->routeRepository = $routeRepository;
    }

    /**
     * @param UserDto $from
     * @return DbUserDto
     * @throws UnknownProperties
     */
    public function getUser(UserDto $from): DbUserDto
    {
        return $this->userRepository->findOrCreate($from);
    }

    /**
     * @param int $userId
     * @return RouteDto
     * @throws UnknownProperties
     */
    public function route(int $userId): RouteDto
    {
        return $this->routeRepository->findOrCreateRoute($userId);
    }
}